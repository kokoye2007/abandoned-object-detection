# Python

- https://github.com/harshagarwal10/abandoned-object-detection

- https://github.com/Atharv24/Abandoned-object-detection

- https://github.com/Grad-CSE2016/Abandoned-Object-Detection

- https://github.com/seniorPro/Intelligent-Surveillance-System-for-Abandoned-Luggage
 - TF

---
## Sample Video
- https://github.com/kevinlin311tw/ABODA


---
# MATLAB

- https://github.com/thesagarsehgal/Abandoned-Object-Detection

---
# C

- https://github.com/kevinlin311tw/ObjLeft

 - https://github.com/kevinlin311tw/ObjLeft_demo
 

- https://github.com/SaranshKejriwal/Abandoned_Object 


---
# Image AI

- https://medium.com/deepquestai/train-object-detection-ai-with-6-lines-of-code-6d087063f6ff

- https://towardsdatascience.com/object-detection-with-10-lines-of-code-d6cb4d86f606


----
# Ref

- https://link.springer.com/content/pdf/10.1155/2008/197875.pdf

- https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6308643/

- https://www.mdpi.com/1424-8220/18/12/4290

- https://www.uit.edu.mm/storage/2020/08/4-A-Study-on-Abandoned-Object-Detection-Methods-in-Video-Surveillance-System.pdf