# Module
- python
- fastai
- pytorch
- opencv
- imutils
- pafy
- youtube-dl

---

# Source
- Video 
- YouTube
- Webcam 
- CCTV (NOT YET)

---

# Label
- Backpack
- Luggage
- Umbrella
- Purse
- Book

---

# MoG
- https://github.com/rmnrajan/unattendedbaggage/blob/master/bg.jpg
- https://github.com/Jeff20/AbandonObject


---

# Original AOD Source
- https://github.com/harshagarwal10/abandoned-object-detection


---

# Timer
-  start timer after human separated from obj
 - (or)
- start timer when human is lost in the scene



